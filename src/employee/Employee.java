/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author Admin
 */
public class Employee {
    
       private String firstName;
       int hr_wage;
       int no_of_hr;
       
 
       Employee(){}
      
      public Employee( String first, int hr, int id)
      {
         firstName = first;
         hr_wage = id;
         no_of_hr = hr;
      } 

      /**
       * 
       * @param first 
       */
      public void setFirstName( String first )
      {
         firstName = first; // should validate
      } // end method setFirstName

      /**
       * 
       * @return 
       */
      public String getFirstName()
      {
         return firstName;
      } // end method getFirstName

    public int getHr_wage() {
        return hr_wage;
    }

    public void setHr_wage(int hr_wage) {
        this.hr_wage = hr_wage;
    }

    public int getNo_of_hr() {
        return no_of_hr;
    }

    public void setNo_of_hr(int no_of_hr) {
        this.no_of_hr = no_of_hr;
    }
    
    public int calculatePay(){
         return hr_wage * no_of_hr;
    }

    @Override
    public String toString() {
        return "Employee{" + "firstName=" + firstName + ", hr_wage=" + hr_wage + ", no_of_hr=" + no_of_hr + '}';
    }
    

   

      

}

