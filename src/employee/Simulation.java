/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author Admin
 */
public class Simulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        Employee Employee =                                 
        new Employee(  "John", 14, 30 );
        Manager Manager = new Manager( "Karen", 16.75, 30, 40 );
         
         System.out.printf( "%s\n%s: $%,.2f\n\n",
            Employee, "earned", Employee.calculatePay() );
         System.out.printf( "%s\n%s: $%,.2f\n\n",
            Manager, "earned", Manager.calculatePay()); 
         
    }
}
